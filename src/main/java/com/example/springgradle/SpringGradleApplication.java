package com.example.springgradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@SpringBootApplication
public class SpringGradleApplication {

  @GetMapping
  public String hello(){
    return  "안녕하세요!";
  }
  public static void main(String[] args) {
    SpringApplication.run(SpringGradleApplication.class, args);
  }

}
